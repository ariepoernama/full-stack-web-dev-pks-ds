<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{   /**
    * index
    *
    * @return void
    */
   public function index(Request $request)
   {   
       $post_id = $request->post_id;
       //get data from table comments
       $comments = Comment::where('post_id' , $post_id)->latest()->get();

       //make response JSON
       return response()->json([
           'success' => true,
           'message' => 'Komentar berhasil ditampilkan',
           'data'    => $comments
       ]);

   }
   
    /**
    * show
    *
    * @param  mixed $id
    * @return void
    */
   public function show($id)
   {
       //find comment by ID
       $comment = Comment::find($id);

       //make response JSON
       if ($comment){
       
               return response()->json([
                   'success' => true,
                   'message' => 'Komentar berhasil ditampilkan',
                   'data'    => $comment
               ], 200);

       }

       return response()->json([
           'success' => false,
           'message' => 'Data dengan id : ' . $id . ' tidak dapat ditampilkan',
       ], 404);
       

   }
   
   /**
    * store
    *
    * @param  mixed $request
    * @return void
    */
   public function store(Request $request)
   {


       $allRequest = $request->all();

       //set validation
       $validator = Validator::make($allRequest, [
           'content'   => 'required',
           'post_id' => 'required',
       ]);
       
       //response error validation
       if ($validator->fails()) {
           return response()->json($validator->errors(), 400);
       }

       //save to database
       $comment = Comment::create([
           'content'   => $request->content,
           'post_id' => $request->post_id,
       ]);

       //success save to database
       if($comment) {

           return response()->json([
               'success' => true,
               'message' => 'Komentar berhasil dibuat',
               'data'    => $comment
           ], 200);

       } 

       //failed save to database
       return response()->json([
           'success' => false,
           'message' => 'Komentar gagal dibuat',
       ], 409);

   }
   
   /**
    * update
    *
    * @param  mixed $request
    * @param  mixed $comment
    * @return void
    */
   public function update(Request $request, $id)
   {
       $allRequest = $request->all();

       //set validation
       $validator = Validator::make($allRequest, [
           'content'   => 'required'
       ]);
       
       //response error validation
       if ($validator->fails()) {
           return response()->json($validator->errors(), 400);
       }

       //find post by ID
       $comment = Comment::find($id);

       if($comment) {

           //update post
           $comment->update([
               'content' => $request->content,
           ]);

           return response()->json([
               'success' => true,
               'message' => 'Komentar berhasil diupdate',
               'data'    => $comment  
           ]);

       }

       //data post not found
       return response()->json([
           'success' => false,
           'message' => 'Komentar tidak ditemukan',
       ], 404);

   }
   
   /**
    * destroy
    *
    * @param  mixed $id
    * @return void
    */
   public function destroy($id)
   {
       //find comment by ID
       $comment = Comment::find($id);

       if($comment) {

           //delete comment
           $comment->delete();

           return response()->json([
               'success' => true,
               'message' => 'Komentar berhasil dihapus',
           ], 200);

       }

       //data comment not found
       return response()->json([
           'success' => false,
           'message' => 'Komentar tidak ditemukan',
       ], 404);
   }
}

<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{    
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table Roles
        $roles = Role::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Role berhasil ditampilkan',
            'data'    => $roles  
        ]);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find comment by ID
        $role = Role::find($id);

        //make response JSON
        if ($role){
        
                return response()->json([
                    'success' => true,
                    'message' => 'Data role berhasil ditampilkan',
                    'data'    => $role
                ], 200);

        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' . $id . ' tidak dapat ditampilkan',
        ], 404);
        

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();

        //set validation
        $validator = Validator::make($allRequest, [
            'name'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $role = Role::create([
            'name'     => $request->name,
        ]);

        //success save to database
        if($role) {

            return response()->json([
                'success' => true,
                'message' => 'Data role berhasil dibuat',
                'data'    => $role  
            ], 200);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Data role gagal dibuat',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $role
     * @return void
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();

        //set validation
        $validator = Validator::make($allRequest, [
            'name'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find role by ID
        $role = Role::find($id);

        if($role) {

            //update role
            $role->update([
                'name'     => $request->name,

            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data berhasil diupdate',
                'data'    => $role  
            ]);

        }

        //data role not found
        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find Role by ID
        $role = Role::find($id);

        if($role) {

            //delete Role
            $role->delete();

            return response()->json([
                'success' => true,
                'message' => 'Role berhasil dihapus',
            ], 200);

        }

        //data Role not found
        return response()->json([
            'success' => false,
            'message' => 'Role tidak ditemukan',
        ], 404);
    }
}
